package com.qualcomm.qcontacts;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.qualcomm.qcontacts.adapter.ContactListAdapter;
import com.qualcomm.qcontacts.model.Contact;
import com.qualcomm.qcontacts.provider.ContactModel;
import com.qualcomm.qcontacts.service.ContactSyncReceiver;
import com.qualcomm.qcontacts.service.ContactSyncService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactSyncReceiver.ContactSyncReceiverHandler, SwipeRefreshLayout.OnRefreshListener {

    ListView mContactListView;
    TextView mEmptyContactTextView;
    List<Contact> contacts;
    ContactListAdapter contactAdapter;
    ContactSyncReceiver receiver;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContactListView = (ListView) findViewById(R.id.contacts_list);
        mEmptyContactTextView = (TextView) findViewById(R.id.no_contacts_view);

        mContactListView.setEmptyView(mEmptyContactTextView);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        receiver = new ContactSyncReceiver(new Handler());
        receiver.setReceiverHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = this.getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_contacts:
                Intent intent = new Intent(this, AddContactActivity.class);
                startActivityForResult(intent, 0);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String name = data.getStringExtra("name");
            String phone = data.getStringExtra("phone");

            contacts.add(new Contact(name, phone));
            contactAdapter.notifyDataSetChanged();
        }
    }

    public void refreshList() {
        contacts = new ArrayList<>();

        Cursor contactCursor = getContentResolver().query(ContactModel.CONTENT_URI, null, null, null, null);
        if (contactCursor != null) {
            while (contactCursor.moveToNext()) {
                Contact contact = new Contact(
                        contactCursor.getString(contactCursor.getColumnIndex(ContactModel.NAME)),
                        contactCursor.getString(contactCursor.getColumnIndex(ContactModel.PHONE)));

                contacts.add(contact);
            }
            contactCursor.close();
        }
        contactAdapter = new ContactListAdapter(contacts, this);

        mContactListView.setAdapter(contactAdapter);
    }

    @Override
    public void onRefresh() {
        Intent intent = new Intent(this, ContactSyncService.class);
        intent.putExtra("contact_sync_receiver", receiver);
        startService(intent);
    }

    @Override
    public void onContactSynced(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case RESULT_OK:
                mSwipeRefreshLayout.setRefreshing(false);
                refreshList();
                break;
        }
    }
}
