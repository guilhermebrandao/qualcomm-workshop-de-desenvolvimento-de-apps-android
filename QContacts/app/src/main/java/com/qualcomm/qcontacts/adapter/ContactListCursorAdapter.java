package com.qualcomm.qcontacts.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.qualcomm.qcontacts.R;
import com.qualcomm.qcontacts.provider.ContactModel;

/**
 * Created by guilherme on 08/12/16.
 */

public class ContactListCursorAdapter extends CursorAdapter {

    LayoutInflater layoutInflater;

    private ContactListListener mListener;

    public ContactListCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return layoutInflater.inflate(R.layout.contacts_list_item, parent);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndex(ContactModel.NAME));
        String phone = cursor.getString(cursor.getColumnIndex(ContactModel.PHONE));

        TextView nameTextView = (TextView) view.findViewById(R.id.contact_name);
        TextView phoneTextView = (TextView) view.findViewById(R.id.contact_phone);
        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.delete_contact_btn);

        nameTextView.setText(name);
        phoneTextView.setText(phone);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.getContentResolver().delete(Uri.withAppendedPath(ContactModel.CONTENT_URI)
            }
        });
    }

    public interface ContactListListener {
        void onDataDelete();
    }
}
