package com.qualcomm.qcontacts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qualcomm.qcontacts.R;
import com.qualcomm.qcontacts.model.Contact;

import java.util.List;

/**
 * Created by guilherme on 08/12/16.
 */

public class ContactListAdapter extends BaseAdapter {

    List<Contact> mContactList;
    Context mContext;
    LayoutInflater inflater;

    public ContactListAdapter(List<Contact> contactList, Context context) {
        this.mContactList = contactList;
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mContactList.size();
    }

    @Override
    public Object getItem(int position) {
        return mContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contacts_list_item, null);
        }

        Contact currentContact = (Contact) getItem(position);

        TextView contactName = (TextView) convertView.findViewById(R.id.contact_name);
        TextView contactPhone = (TextView) convertView.findViewById(R.id.contact_phone);

        contactName.setText(currentContact.getName());
        contactPhone.setText(currentContact.getPhone());

        return convertView;
    }
}


