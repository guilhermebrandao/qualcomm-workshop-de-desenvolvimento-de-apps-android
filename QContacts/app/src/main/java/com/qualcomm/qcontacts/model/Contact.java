package com.qualcomm.qcontacts.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by guilherme on 07/12/16.
 */

public class Contact {

    @SerializedName("name")
    private String name;

    @SerializedName("phone")
    private String phone;

    public Contact() {}

    public Contact (final String pName, final String pPhone) {
        this.name = pName;
        this.phone = pPhone;
    }

    public final String getName() {
        return name;
    }

    public void setName(final String pName) {
        this.name = pName;
    }

    public final String getPhone() {
        return phone;
    }

    public void setPhone(final String pPhone) {
        this.phone = pPhone;
    }

    @Override
    public final String toString() {
        return this.name + " - " + this.phone;
    }
}
