package com.qualcomm.qcontacts.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by guilherme on 08/12/16.
 */

public class ContactSyncReceiver extends ResultReceiver {

    public ContactSyncReceiver(Handler handler) {
        super(handler);
    }

    private ContactSyncReceiverHandler receiverHandler;

    public interface ContactSyncReceiverHandler {
        void onContactSynced(int resultCode, Bundle resultData);
    }

    public void setReceiverHandler(ContactSyncReceiverHandler receiverHandler) {
        this.receiverHandler = receiverHandler;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiverHandler != null) {
            receiverHandler.onContactSynced(resultCode, resultData);
        }
    }

}
