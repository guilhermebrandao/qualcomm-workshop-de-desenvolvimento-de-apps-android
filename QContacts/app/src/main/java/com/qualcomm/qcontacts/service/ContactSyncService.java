package com.qualcomm.qcontacts.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.gson.Gson;
import com.qualcomm.qcontacts.model.Contact;
import com.qualcomm.qcontacts.network.ContactsHttpManager;
import com.qualcomm.qcontacts.provider.ContactModel;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by guilherme on 08/12/16.
 */

public class ContactSyncService extends IntentService {

    public ContactSyncService() {
        super("ContactSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ContactsHttpManager httpManager = new ContactsHttpManager();
        String result = "";

        try {
            result = httpManager.getContacts();
            List<Contact> contacts = parseContacts(result);
            syncDatabase(contacts);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResultReceiver rec = intent.getParcelableExtra("contact_sync_receiver");
        Bundle bundle = new Bundle();
        if (rec != null) {
            rec.send(Activity.RESULT_OK, bundle);
        }


    }

    private List<Contact> parseContacts(String rawContacts) {
        if (rawContacts == null) return null;

        Gson gson = new Gson();
        Contact[] contacts = gson.fromJson(rawContacts, Contact[].class);
        return Arrays.asList(contacts);

    }

    private void syncDatabase(List<Contact> contacts) {
        int deletedRows = getContentResolver().delete(ContactModel.CONTENT_URI, null, null);

        for (Contact contact : contacts) {
            ContentValues values = new ContentValues();
            values.put(ContactModel.NAME, contact.getName());
            values.put(ContactModel.PHONE, contact.getPhone());
            getContentResolver().insert(ContactModel.CONTENT_URI, values);
        }
    }
}
