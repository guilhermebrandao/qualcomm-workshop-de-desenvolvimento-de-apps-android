package com.qualcomm.qcontacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.qualcomm.qcontacts.network.ContactsHttpManager;
import com.qualcomm.qcontacts.service.ContactSyncService;

public class AddContactActivity extends AppCompatActivity {

    EditText mNameTextView, mPhoneTextView;
    Button mAddContactButton;
    View.OnClickListener addContactClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addContact();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        mNameTextView = (EditText) findViewById(R.id.name_edit_text);
        mPhoneTextView = (EditText) findViewById(R.id.phone_edit_text);
        mAddContactButton = (Button) findViewById(R.id.btn_insert_contact);

        mAddContactButton.setOnClickListener(addContactClick);
    }

    private void addContact() {
        String name = mNameTextView.getText().toString();
        String phone = mPhoneTextView.getText().toString();

        boolean isValid = true;

        Animation animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);

        if (TextUtils.isEmpty(name)) {
            mNameTextView.setError(getString(R.string.invalid));
            mNameTextView.setAnimation(animation);
            isValid = false;
        }

        if (TextUtils.isEmpty(phone) || !Patterns.PHONE.matcher(phone).matches()) {
            mPhoneTextView.setError(getString(R.string.invalid));
            mNameTextView.setAnimation(animation);
            isValid = false;
        }

        if (isValid) {
            ContactsHttpManager httpManager = new ContactsHttpManager();
            httpManager.sendContact(name, phone);

            Intent intent = new Intent(this, ContactSyncService.class);
            startService(intent);

            finish();
        }


    }
}
