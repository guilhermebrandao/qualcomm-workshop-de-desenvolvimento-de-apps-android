package com.qualcomm.qcontacts.provider;

import android.net.Uri;
import android.provider.BaseColumns;

import com.qualcomm.qcontacts.QContacts;

/**
 * Created by guilherme on 07/12/16.
 */

public class ContactModel implements BaseColumns {
    public static final String TABLE_NAME = "contacts";
    public static final String NAME = "name";
    public static final String PHONE = "phone";

    public static final Uri CONTENT_URI = Uri.parse("content://" + QContacts.AUTHORITY + "/contact");
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.qcontact.contact";
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.qcontact.contact";
    public static final String CREATE_CONTACT_TABLE = "create table "
            + TABLE_NAME + "("
            + _ID + " integer primary key autoincrement, "
            + NAME + " text not null, "
            + PHONE + " text not null);";
}
