package com.qualcomm.qcontacts.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by guilherme on 07/12/16.
 */

public class QContactsDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "qcontacts.db";
    public static final int DATABASE_VERSION = 1;

    public QContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ContactModel.CREATE_CONTACT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            switch (oldVersion) {
                case 1:
            }
        }
    }
}
