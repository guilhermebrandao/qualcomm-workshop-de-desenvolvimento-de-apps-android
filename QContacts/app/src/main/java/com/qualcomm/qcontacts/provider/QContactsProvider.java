package com.qualcomm.qcontacts.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.qualcomm.qcontacts.QContacts;

/**
 * Created by guilherme on 07/12/16.
 */

public class QContactsProvider extends ContentProvider {

    public static final String TAG = "QContacts";

    public static final int URI_CONTACT = 1;
    public static final int URI_CONTACT_ID = 2;
    public static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(QContacts.AUTHORITY, "contact", URI_CONTACT);
        URI_MATCHER.addURI(QContacts.AUTHORITY, "contact/#", URI_CONTACT_ID);
    }

    private QContactsDatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new QContactsDatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case URI_CONTACT:
                queryBuilder.setTables(ContactModel.TABLE_NAME);
                break;
            case URI_CONTACT_ID:
                queryBuilder.setTables(ContactModel.TABLE_NAME);
                queryBuilder.appendWhere(ContactModel._ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown or not supported URI: " + uri);
        }

        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor c = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case URI_CONTACT:
                return ContactModel.CONTENT_TYPE;

            case URI_CONTACT_ID:
                return ContactModel.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    private Uri insertContact(ContentValues values) {
        Uri returnUri = null;

        try {
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            long id = database.insert(ContactModel.TABLE_NAME, null, values);

            if (id > 0) {
                returnUri = ContentUris.withAppendedId(ContactModel.CONTENT_URI, id);
                getContext().getContentResolver().notifyChange(returnUri, null);
            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not write to DB: " + e.getMessage());
        }
        return returnUri;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (URI_MATCHER.match(uri)) {
            case URI_CONTACT:
                return insertContact(values);
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int count;
        String id;

        switch (URI_MATCHER.match(uri)) {
            case URI_CONTACT_ID:
                id = uri.getLastPathSegment();
                count = database.delete(ContactModel.TABLE_NAME, ContactModel._ID + "=" + id
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : ""), selectionArgs);
                break;
            case URI_CONTACT:
                count = database.delete(ContactModel.TABLE_NAME, null, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown or not supported URI: " + uri);
        }
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;

        try {
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            String id = null;

            switch (URI_MATCHER.match(uri)) {
                case URI_CONTACT_ID:
                    id = uri.getLastPathSegment();
                    count = database.update(ContactModel.TABLE_NAME, values, ContactModel._ID + "=" + id
                            + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : ""), selectionArgs);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown or not supported URI: " + uri);

            }
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not write to DB: " + e.getMessage());
        }
        return count;
    }
}
