package com.qualcomm.qcontacts.network;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by guilherme on 08/12/16.
 */

public class ContactsHttpManager {

    public static final String CONTACTS_URL = "https://contatostreinamento.herokuapp.com/contacts";

    public String postContact(String pName, String pPhone) throws IOException {
        OutputStreamWriter writer = null;
        String contentAsString = "";

        try {
            URL url = new URL(CONTACTS_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestProperty("Content-type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            JSONObject data = new JSONObject();
            data.put("name", pName);
            data.put("phone", pPhone);
            JSONObject contact = new JSONObject();
            contact.put("contact", data);

            writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(contact.toString());
            writer.flush();

            StringBuilder builder = new StringBuilder();
            int httpResult = conn.getResponseCode();
            if (httpResult == HttpURLConnection.HTTP_CREATED || httpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line + "\n");
                }
                reader.close();
                contentAsString = builder.toString();
            } else {
                contentAsString = conn.getResponseMessage();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        return contentAsString;
    }

    public String getContacts() throws IOException {
        OutputStreamWriter writer = null;
        String contentAsString = "";

        try {
            URL url = new URL(CONTACTS_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestProperty("Content-length", "0");
            conn.setRequestMethod("GET");
            conn.setAllowUserInteraction(false);
            conn.setUseCaches(false);
            conn.connect();

            StringBuilder builder = new StringBuilder();
            int httpResult = conn.getResponseCode();
            if (httpResult == HttpURLConnection.HTTP_CREATED || httpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line + "\n");
                }
                reader.close();
                contentAsString = builder.toString();
            } else {
                contentAsString = conn.getResponseMessage();
            }

        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        return contentAsString;
    }

    /**
     * Executa AsyncTask.
     * @param pName Nome do contato a ser enviado.
     * @param pPhone Telefone do contato a ser enviado.
     */
    public void sendContact(String pName, String pPhone) {
        SendContactAsyncTask sendTask = new SendContactAsyncTask(pName, pPhone);
        sendTask.execute();
    }

    /**
     * Executa AsyncTask de download dos contatos.
     */
    public void downloadContacts() {
        DownloadContactsAsyncTask downloadTask = new DownloadContactsAsyncTask();
        downloadTask.execute();
    }

    /**
     * Classe para enviar os contatos.
     */
    private class SendContactAsyncTask extends AsyncTask<String, Void, String> {

        private String name;
        private String phone;

        SendContactAsyncTask(String pName, String pPhone) {
            this.name = pName;
            this.phone = pPhone;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return postContact(name, phone);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    /**
     * Classe para fazer o download dos contatos.
     */
    private class DownloadContactsAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return getContacts();
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }
}
